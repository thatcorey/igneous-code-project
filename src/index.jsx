import Inferno from 'inferno';
import Root from './components/Root'

const container = document.getElementById('app');

Inferno.render(<Root> </Root>, container);
