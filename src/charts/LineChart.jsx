import Inferno from 'inferno'
import Component from 'inferno-component'

import { getData } from '../components/api'

import { line } from 'd3-shape'
import { scaleLinear } from 'd3-scale'
import { max } from 'd3-array'
import { select } from 'd3-selection'

class LineChart extends Component {

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        const { name } = this.props.config;
        getData(name, 'today').then((result) => {
            this.setState({data: result.data}, (v) => {
                this.createChart();
            })
        })
    }

    createChart() {
        const node = this.node
        const values = this.state.data.map((o) => o.value);
        const dataMax = max(values)

        const lineFunc = line()
            .x((d, i) => i * 25)
            .y((d) => yScale(d));

        const yScale = scaleLinear()
            .domain([0, dataMax])
            .range([0, values.length])


        select(node)
            .append("path")
            .data([values])
            .attr("stroke", "blue")
            .attr("stroke-width", 2)
            .attr("fill", "none")
            .attr("d", lineFunc);
    }

    render() {

        console.log(this.props)
        return <div className="lineChart">
            { this.props.type }
            <svg ref={node => this.node = node} width={500} height={50} />
        </div>
    }
}
export default LineChart;