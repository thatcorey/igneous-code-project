import Inferno from 'inferno'
import Component from 'inferno-component'
import { getData } from '../components/api'
import { scaleLinear } from 'd3-scale'
import { max } from 'd3-array'
import { select } from 'd3-selection'

class BarChart extends Component {

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        const { name } = this.props.config;
        getData(name, 'yesterday').then((result) => {
            this.setState({data: result.data}, (v) => {
                this.createChart();
            })
        })
    }

    createChart() {
        const node = this.node
        const values = this.state.data.map((o) => o.value);
        const dataMax = max(values)
        const yScale = scaleLinear()
            .domain([0, dataMax])
            .range([0, values.length])


        select(node)
            .selectAll('rect')
            .data(values)
            .enter()
            .append('rect')

        select(node)
            .selectAll('rect')
            .data(values)
            .style('fill', '#fe9922')
            .attr('x', (d,i) => i * 25)
            .attr('y', d => {
                return 50- yScale(d)
            })
            .attr('height', d => yScale(d))
            .attr('width', 25)
    }

    render() {
        return <div className="barChart">
            <svg ref={node => this.node = node} width={500} height={50} />
        </div>
    }
}
export default BarChart