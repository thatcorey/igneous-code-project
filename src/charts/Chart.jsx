import Inferno from 'inferno'
import Component from 'inferno-component'

import BarChart from './BarChart'
import LineChart from './LineChart'

import './charts.css'

const chartTypes = {
    'bar' : BarChart,
    'line': LineChart
};

class Chart extends Component {

    render() {
        const { chart } = this.props;
        const InnerChart = chartTypes[chart.type];

        return <div className="chartContainer">
            { InnerChart && <InnerChart config={chart} /> }
            <div className="chartTitle">{chart.name}</div>
        </div>
    }
}
export default Chart