import axios from 'axios';

const BASEURL = '/api/v1/';

    export const getData = (component, timerange) =>
        axios.get(`${BASEURL}${component}/${timerange}`)
            .then((response) => response.data);