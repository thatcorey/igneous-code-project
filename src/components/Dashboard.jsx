import Inferno from 'inferno';
import Component from 'inferno-component';

import Chart from '../charts/Chart';

class Dashboard extends Component {

    render() {
        const { charts = [] } = this.props;
        return <div className="dashboard" hasKeyedChildren>{ 
            charts && charts.map((chart) => <Chart key={chart.id} chart={chart} />)
        }</div>
    }

}

export default Dashboard;