import Inferno from 'inferno';
import Component from 'inferno-component';
import shortid from 'shortid';
import Dashboard from './Dashboard'

// temp state
const monitoredComponents = ['CPU', 'Memory', 'Disk', 'Network'];
const chartTypes = ['line', 'bar'];

class Root extends Component {

    constructor(props) {
        super(props); 
        this.state = {
            charts: []
        }
    }

    componentDidMount() {
        // this just assigns random chart types to the monitored components
        const charts = monitoredComponents.map(
            (o, i) => ({ id: shortid.generate(), name: o, type: chartTypes[Math.floor(Math.random() * chartTypes.length)] })
        );
        this.setState({ charts });
    }

    render() {
        const { charts } = this.state;
        return <Dashboard charts={charts} />
    }

}

export default Root;
