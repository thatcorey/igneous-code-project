# React Babel Webpack with a hint of Golang

This is an as simple as possible ES6/Webpack/Inferno project with Go server. 
The purpose of this is for quickly prototyping with all the goodness of the NPM system at your disposal. 

## Setup Guide

`glide install`

`yarn`

`yarn start`

Webpack is now watching your project for changes and a Go server is running.

Visit `http://localhost:8080` and you should be good to go!



### You'll need this stuff if you have nothing installed
Install Go - https://golang.org/