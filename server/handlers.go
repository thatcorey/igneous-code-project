package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"
	"log"
)

func Index(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("index.html")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Error: %s", err)
	}

	if pusher, ok := w.(http.Pusher); ok {
		// Push is supported.
		if err := pusher.Push("/dist/bundle.js", nil); err != nil {
			log.Printf("Failed to push: %v", err)
		}
	}

	t.Execute(w, nil)
}

func API(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var component string
	var daterange string
	component = vars["component"]
	daterange = vars["daterange"]
	dataset := DoQuery(component, daterange)
	if len(dataset.Data) > 0 {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(dataset); err != nil {
			panic(err)
		}
		return
	}

	// If we didn't find it, 404
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNotFound)
	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusNotFound, Text: "Not Found"}); err != nil {
		panic(err)
	}

}