package main

import (
	"log"
	"net/http"
)

func main() {
	port := ":8080"
	r := NewRouter()
	s := http.StripPrefix("/dist/", http.FileServer(http.Dir("./dist/")))
	r.PathPrefix("/dist/").Handler(s)
	log.Printf("### Server started on %s ###", port)
	log.Fatal(http.ListenAndServe(":8080", r))
}



