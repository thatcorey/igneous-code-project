package main

// import "fmt"
import (
	"math/rand"
	"time"
	"math"
)

var r = rand.New(rand.NewSource(time.Now().UnixNano()))

func DoQuery(component string, daterange string) Dataset {
	// return empty Dataset if not found

	dataset := Dataset{}
	dataset.Name = daterange
	ref := time.Now();
	switch daterange {
	case "today":
		t := time.Date(ref.Year(), ref.Month(), ref.Day(), ref.Hour(), 0, 0, 0, ref.Location())
		for  i := 0; i < 24; i++ {
			t = t.Add(-1 * time.Hour)
			dataset.Data = append(dataset.Data, Datapoint{t, math.Floor(r.Float64()*1000)})
	}

	case "yesterday":
		t := time.Date(ref.Year(), ref.Month(), ref.Day(), ref.Hour(), 0, 0, 0, ref.Location())
		for  i := 0; i < 48; i++ {
			t = t.Add(-1 * time.Hour)
			dataset.Data = append(dataset.Data, Datapoint{t, math.Floor(r.Float64()*1000)})
	}
	default:
		dataset.Data = []Datapoint{}
	}
	return dataset
}