package main

import (
	"time"
)

type Dataset struct {
	Id        int       	`json:"id"`
	Name      string    	`json:"name"`
	Data	  []Datapoint	`json:"data"`
}

type Datapoint struct {
	Timestamp 	time.Time 	`json:"timestamp"`
	Value		float64		`json:"value"`
}
